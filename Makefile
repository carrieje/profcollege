.PHONY: example test compare-gif

example: example.pdf

example.pdf: example.tex
	pdflatex -shell-escape $<

test:
	@cd tests; rm out/* diffs/*;
	@cd tests; for f in *.tex; do \
		pdflatex -shell-escape "$${f}"; \
		pdfcrop $${f%.tex}.pdf; \
		mv $${f%.tex}-crop.pdf out/$${f%.tex}.pdf; \
		rm $${f%.tex}.pdf; \
		magick compare -density 300 {out,expected,diffs}/$${f%.tex}.pdf; \
	done
	@cd tests; rm *.log *.aux *.mp *.mps *.mpx

compare-gif:
	@cd tests; for f in *.tex; do \
		magick -delay 50 -density 300 -loop 0 -dispose 3 {out,expected}/$${f%.tex}.pdf diffs/$${f%.tex}.gif; \
	done
